import bpy
import urllib.request
import json
import zipfile
import os
import bmesh
from bpy.types import Operator

from . import utils

from bpy.props import (
        StringProperty,
        BoolProperty,
        IntProperty,
        FloatProperty,
        FloatVectorProperty,
        EnumProperty,
        )

tileset = {}

# Fetch and store tilemap
def get_tiles_in_basemap(error_margin):
    get_tileset(error_margin)
    global tileset

    bboxes = list()
    basemap = utils.get_basemap_obj()
    shiftLon = basemap.location.x
    shiftLat = basemap.location.y
    lon = bpy.context.scene["longitude"]
    lat = bpy.context.scene["latitude"]

    rd = utils.Rijksdriehoek()
    rd.from_wgs(lat, lon)
    lon = rd.rd_x + shiftLon
    lat = rd.rd_y + shiftLat

    sscale = utils.get_scene_scale_m()
    error = error_margin
    worldRect = [lon - sscale[0]/2 - error, lat - sscale[1]/2 - error, lon + sscale[0]/2 + error, lat + sscale[1]/2 + error]

    # Pas de nieuwe structuur aan
    for feature in tileset.get('features', []):
        bbox = feature["bbox"]
        uri = feature["properties"].get("obj_download", "")
        if isRectangleOverlap(bbox, worldRect):
            bboxes.append([bbox, uri])
    return bboxes

# Get current tileset via BBOX api
def get_tileset(error_margin):
    global tileset
    if tileset:
        print("tileset already cached")
        return
    print("Fetching tileset with new bbox API")
    
    # Bounding box ophalen en API-request met bbox
    basemap = utils.get_basemap_obj()
    lon, lat = bpy.context.scene["longitude"], bpy.context.scene["latitude"]
    rd = utils.Rijksdriehoek()
    rd.from_wgs(lat, lon)
    lon, lat = rd.rd_x + basemap.location.x, rd.rd_y + basemap.location.y
    sscale = utils.get_scene_scale_m()
    error = error_margin
    bbox = f"{lon - sscale[0]/2 - error},{lat - sscale[1]/2 - error},{lon + sscale[0]/2 + error},{lat + sscale[1]/2 + error}"

    # URL samenstellen voor nieuwe API-oproep
    url = f"https://data.3dbag.nl/api/BAG3D/wfs?version=1.1.0&request=GetFeature&typename=BAG3D:Tiles&outputFormat=application/json&srsname=EPSG:28992&bbox={bbox},EPSG:28992"
    print('getting tileset from ULR: ' + url)        

    try:
        req = urllib.request.Request(url, None, utils.get_request_header())
        handle = urllib.request.urlopen(req, timeout=10)
        data = handle.read()
        handle.close()
        tileset = json.loads(data)
    except Exception as e:
        print(f"Error fetching tileset: {e}")
        tileset = {}

def isRectangleOverlap(R1, R2):
    return not (R1[0] >= R2[2] or R1[2] <= R2[0] or R1[3] <= R2[1] or R1[1] >= R2[3])

class op_get_bag(Operator):
    bl_idname = "gis.get_bag"
    bl_label = "Get 3D BAG"
    bl_description = "Download 3D buildings to match your existing basemap"
    bl_options = {'REGISTER', 'UNDO'}
    
    bag_lod: EnumProperty(
        name="Detail",
        description="3D BAG comes in different levels of detail.\nLOD 1.2 is the lowest, with each building being just one shape\nLOD 1.3 can have building split up in different heights\nLOD 2.2 has defined roof shapes",
        items=(
            ('lod12', "LOD 1.2 (low detail)", ""),
            ('lod13', "LOD 1.3 (medium detail)", ""),
            ('lod22', "LOD 2.2 (high detail)", "")
        ),
        default='lod22',
    )
    
    excess_cleanup: EnumProperty(
        name="Excess",
        description="The downloaded 3D BAG tiles are bigger than the basemap. What to do with the excess?",
        items=(
            ('CUT', "Cut away", "This cuts away all the excess geometry outside the basemaps's bounds"),
            ('INCLUDE', "Remove Inclusive", "This keeps all buildings that touch the basemap (some overhang)"),
            ('EXCLUDE', "Remove Exclusive", "This removes all the buildings that are not entirely on the basemap (no overhang)"),
            ('NOTHING', "Do nothing", "Keeps the tiles intact")
        ),
        default='CUT',
    )
    
    error_margin: FloatProperty(
        name="Extra margin for tile download",
        description="(advanced) Sometimes too few tiles are pulled, increase this value until it works.",
        default=50,
    )

    def invoke(self, context, event):
        global tileset
        tileset = {}
        print('clearing tileset')
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=250)

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        if(utils.is_scene_georef()):
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="Get Dutch 3D buildings to", icon='INFO')
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="match your existing basemap")
            layout.separator()
            layout.row()
            layout.row()
            
            row = layout.row()
            row.label(text="Download:", icon='URL')
            row = layout.row()
            row.prop(self, "bag_lod")
            row = layout.row()
            row.prop(self, "error_margin")
            numtiles = len(get_tiles_in_basemap(self.error_margin))
            row = layout.row()             
            row.scale_y = 0.6
            row.label(text="Tiles in basemap: " + str(numtiles))
            
            layout.row()
            layout.row()
                  
            row = layout.row()
            row.label(text="Mesh:", icon='MOD_DISPLACE')
            row = layout.row()
            row.prop(self, "excess_cleanup")
            
        else:
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="Scene has no basemap!", icon='ERROR')
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="use (GIS > Web geodata > Basemap)")

    def execute(self, context):
        print("\n\n########## BLENDER HOOGTEDATA ADDON ##########\n\n")
        utils.ensure_basemap_scale()
        bboxes = get_tiles_in_basemap(self.error_margin)
        
        print("Number of tiles in search: " + str(len(bboxes)))

        for b in bboxes:
            # Gebruik alleen het laatste deel van de URI als id                         
            id = os.path.basename(b[1]).replace("-obj.zip", "")
            print(f"\n\nGetting 3D BAG tile: {b[1]}")
            try:
                req = urllib.request.Request(b[1], None, utils.get_request_header())
                handle = urllib.request.urlopen(req, timeout=10)
                data = handle.read()
                handle.close()
                
                file_path = os.path.join(utils.get_tmp_path(), f"bag3d_{id}.zip")
                print(f"Debugging file_path path: {file_path}")
                tmpzipfolder = os.path.join(utils.get_tmp_path(), f"bag3d_{id}/")
                print(f"Debugging tmpzipfolder path: {tmpzipfolder}")
                print(f"utils.get_tmp_path(): {utils.get_tmp_path()}")                

                # Zorg ervoor dat het tijdelijke pad bestaat
                os.makedirs(tmpzipfolder, exist_ok=True)
                print(f"Created temporary folder if it didn't exist: {tmpzipfolder}")
                                                    
                with open(file_path, 'wb') as zip_file:
                    zip_file.write(data)

                print(f"Selected LOD: {self.bag_lod}")  # Debug de geselecteerde LOD                
                with zipfile.ZipFile(file_path, 'r') as zip_ref:
                    zip_ref.extractall(tmpzipfolder)
                    for obj_file in os.listdir(tmpzipfolder):
                        print(f"Checking file: {obj_file}")  # Debug de bestandsnaam
                        if str(self.bag_lod.lower()) in obj_file.lower() and obj_file.lower().endswith('obj'):
                            # Oude Blender 2.x en 3.x methode::
                            #bpy.ops.import_scene.obj(filepath=os.path.join(tmpzipfolder, obj_file), use_split_objects=False, use_split_groups=False, axis_forward='Y', axis_up='Z')
                            # Nieuwe Blender 4.x methode:
                            bpy.ops.wm.obj_import(filepath=os.path.join(tmpzipfolder, obj_file), use_split_objects=False, use_split_groups=False, forward_axis='Y', up_axis='Z')
                
                      
                print("Transforming all OBJ's")
                
                lon = bpy.context.scene["longitude"]
                lat = bpy.context.scene["latitude"]
                
                print("Coordinates: " + str(lon) + ", " + str(lat))

                rd = utils.Rijksdriehoek()
                rd.from_wgs(lat,lon)
                
                bag_objs = [obj for obj in bpy.context.scene.objects if "-LoD" in obj.name]
                print("Number of imported OBJs: " + str(len(bag_objs)))
                
                if(len(bag_objs) == 0):
                    return {'CANCELLED'}
                
                bpy.ops.object.select_all(action='DESELECT')        

                for b in bag_objs:
                    b.location = (-rd.rd_x ,-rd.rd_y, 0)
                    b.select_set(True)
                    bpy.context.view_layer.objects.active = b
                    
                print("Joining the OBJ's")

                bpy.ops.object.join()
                bpy.context.view_layer.objects.active.name = bpy.context.view_layer.objects.active.name.replace('-LoD', '-LoD-Transformed') #to avoid confusion with later added objects.
                
                bpy.ops.object.mode_set(mode = 'EDIT') 
        
                bpy.ops.mesh.select_all(action='SELECT')

                
                basemap = utils.get_basemap_obj()

                cut_x = basemap.location.x
                cut_y = basemap.location.y
                sscale = utils.get_scene_scale_m()
                
                print("Processing joined OBJ according to: " + str(self.excess_cleanup))
                
                if(self.excess_cleanup == 'CUT'):
                    bpy.ops.mesh.select_all(action='SELECT')
                    bpy.ops.mesh.bisect(plane_co=(cut_x - sscale[0]/2, 0, 0), plane_no=(-1, 0, 0), use_fill=False, clear_outer=True)
                    bpy.ops.mesh.select_all(action='SELECT')
                    bpy.ops.mesh.bisect(plane_co=(cut_x + sscale[0]/2, 0, 0), plane_no=(1, 0, 0), use_fill=False, clear_outer=True)
                    bpy.ops.mesh.select_all(action='SELECT')
                    bpy.ops.mesh.bisect(plane_co=(0, cut_y - sscale[1]/2, 0), plane_no=(0, -1, 0), use_fill=False, clear_outer=True)
                    bpy.ops.mesh.select_all(action='SELECT')
                    bpy.ops.mesh.bisect(plane_co=(0, cut_y + sscale[1]/2, 0), plane_no=(0, 1, 0), use_fill=False, clear_outer=True)
                
                elif(self.excess_cleanup == 'NOTHING'):
                    print('do nothing')
                
                else:
                    ob = bpy.context.object
                    me = ob.data
                    bm = bmesh.from_edit_mesh(me)
                    bm.select_mode = {'VERT'}
                    bpy.ops.mesh.select_all(action='DESELECT')
                    for i, v in enumerate(bm.verts):
                        if v.co.x > cut_x - sscale[0]/2 and v.co.x < cut_x + sscale[0]/2 and v.co.y > cut_y - sscale[1]/2 and v.co.y < cut_y + sscale[1]/2:
                            v.select_set(True)
                    if(self.excess_cleanup == 'EXCLUDE'):
                        bpy.ops.mesh.select_all(action='INVERT')
                    bpy.ops.mesh.select_linked()
                    if(self.excess_cleanup == 'INCLUDE'):      
                        bpy.ops.mesh.select_all(action='INVERT')
                    bpy.ops.mesh.delete(type='VERT')
                    bmesh.update_edit_mesh(me)            
                
                bpy.ops.object.mode_set(mode = 'OBJECT') 


                
            except Exception as e:
                print(f"Error with tile {id}: {e}")

        print("\n\n########## # ##########\n\n")
        return {'FINISHED'}
