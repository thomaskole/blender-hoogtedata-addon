import bpy
import math
import os

def get_request_header():
    headers = {
    'Accept' : 'image/png,image/*;q=0.8,*/*;q=0.5' ,
    'Accept-Charset' : 'ISO-8859-1,utf-8;q=0.7,*;q=0.7' ,
    'Accept-Language' : 'fr,en-us,en;q=0.5' ,
    'Proxy-Connection' : 'keep-alive',
    'User-Agent' : "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0",
    'Referer' : ''}
    return headers
    
def ensure_basemap_scale():
    if is_scene_georef():
        basemap = get_basemap_obj()
        if(basemap.empty_display_size == 1):
            basemap.empty_display_size = get_mercator_scale()
            basemap.location.x *= get_mercator_scale()
            basemap.location.y *= get_mercator_scale()
            basemap.location.z = 0
            
    sscale = get_scene_scale_m()
    maxscale = max(sscale[0], sscale[1])
            
    bpy.context.space_data.clip_end = maxscale*5
    bpy.context.space_data.clip_start = maxscale/1000

def get_mercator_scale():
    return math.cos(math.pi * bpy.context.scene['latitude'] / 180)

def is_scene_georef():
    if("longitude" not in bpy.context.scene):
        print("no longitude found")
        return False
    bm = get_basemap_obj()
    if bm is None:
        print("no basemap found")
        return False
    
    return True
    


def get_basemap_obj():
    scn = bpy.context.scene
    objs = [obj for obj in scn.collection.all_objects if obj.empty_display_type == 'IMAGE']
    for bm in objs:
        if '_WM' in bm.name and not bm.hide_viewport:
            return bm
    return None
    
def get_scene_scale_m():
    if not is_scene_georef():
        return None
    
    bm = get_basemap_obj()
    
    img = bm.data
    desiredImageScale = get_mercator_scale()
    aspect = img.size[0] / img.size[1]
    
    scale = bm.scale.x * desiredImageScale
    scaleX = scale
    scaleY = scale
    if(aspect > 1):
        scaleY /= aspect
    else:
        scaleX *= aspect
    
    return scaleX, scaleY
    
def get_scene_resolution_px(resolution_percentage, meters_per_pixel):
    if not is_scene_georef():
        return None
    scene_scale = get_scene_scale_m()
    scene_res_x = scene_scale[0] * resolution_percentage * 0.01 * (1/meters_per_pixel)
    scene_res_y = scene_scale[1] * resolution_percentage * 0.01 * (1/meters_per_pixel)
    return int(scene_res_x), int(scene_res_y)

def get_tmp_path():
    tmp_path = os.path.join(os.path.dirname(os.path.normpath(bpy.app.tempdir)), "nlgis")
    return tmp_path
    
         
#######################

# Rijksdriehoek by Flimpie
# License: GNU General Public License v3 (GPLv3)
# https://pypi.org/project/rijksdriehoek/

class Rijksdriehoek:
    def __init__(self, rd_x = None, rd_y = None):
        self.rd_x = rd_x
        self.rd_y = rd_y
        self.X0 = 155000
        self.Y0 = 463000
        self.PHI0 = 52.15517440
        self.LAM0 = 5.38720621

    def from_wgs(self, lat, lon):
        self.rd_x, self.rd_y = self.__to_rd(lat, lon)

    def to_wgs(self,):
        return self.__to_wgs(self.rd_x, self.rd_y)

    def __to_rd(self, latin, lonin):
        # based off of https://github.com/djvanderlaan/rijksdriehoek
        pqr = [(0, 1, 190094.945),
            (1, 1, -11832.228),
            (2, 1, -114.221),
            (0, 3, -32.391),
            (1, 0, -0.705),
            (3, 1, -2.34),
            (1, 3, -0.608),
            (0, 2, -0.008),
            (2, 3, 0.148)]
        
        pqs = [(1, 0, 309056.544),
            (0, 2, 3638.893),
            (2, 0, 73.077),
            (1, 2, -157.984),
            (3, 0, 59.788),
            (0, 1, 0.433),
            (2, 2, -6.439),
            (1, 1, -0.032),
            (0, 4, 0.092),
            (1, 4, -0.054)]

        dphi = 0.36 * ( latin - self.PHI0 )
        dlam = 0.36 * ( lonin - self.LAM0 )

        X = self.X0
        Y = self.Y0

        for p, q, r in pqr:
            X += r * dphi**p * dlam**q 

        for p, q, s in pqs:
            Y += s * dphi**p * dlam**q

        return [X,Y]

    def __to_wgs(self, xin, yin):
        # based off of https://github.com/djvanderlaan/rijksdriehoek

        pqk = [(0, 1, 3235.65389),
            (2, 0, -32.58297),
            (0, 2, -0.24750),
            (2, 1, -0.84978),
            (0, 3, -0.06550),
            (2, 2, -0.01709),
            (1, 0, -0.00738),
            (4, 0, 0.00530),
            (2, 3, -0.00039),
            (4, 1, 0.00033),
            (1, 1, -0.00012)]

        pql = [(1, 0, 5260.52916), 
            (1, 1, 105.94684), 
            (1, 2, 2.45656), 
            (3, 0, -0.81885), 
            (1, 3, 0.05594), 
            (3, 1, -0.05607), 
            (0, 1, 0.01199), 
            (3, 2, -0.00256), 
            (1, 4, 0.00128), 
            (0, 2, 0.00022), 
            (2, 0, -0.00022), 
            (5, 0, 0.00026)]

        dx = 1E-5 * ( xin - self.X0 )
        dy = 1E-5 * ( yin - self.Y0 )
        
        phi = self.PHI0
        lam = self.LAM0

        for p, q, k in pqk:
            phi += k * dx**p * dy**q / 3600

        for p, q, l in pql:
            lam += l * dx**p * dy**q / 3600

        return [phi,lam]

    ################
