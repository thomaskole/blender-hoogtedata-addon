bl_info = {
        'name': 'Blender Hoogtedata Addon',
        'author': 'Thomas Kole',
        'version': (0, 1),
        'blender': (3, 0, 0),
        'category': 'GIS'
        #'description': 'Converts a mesh with seams into sewing patterns with sewing edges',
        #'location': 'Object > Seams to Sewing Pattern > ...',
        #'wiki_url': 'https://blenderartists.org/t/1248713'
        }

 
from . import op_get_ahn
from . import op_get_bag
from . import op_misc


import bpy
from bpy.types import Menu


def menu_func(self, context):
    lay_out = self.layout
    lay_out.operator_context = 'INVOKE_REGION_WIN'

    lay_out.separator()
    lay_out.menu("VIEW3D_MT_menu_hoogtedata",
                text="Hoogtedata",  icon="AXIS_TOP")
    
class VIEW3D_MT_menu_hoogtedata(Menu):
    bl_idname = "VIEW3D_MT_menu_hoogtedata"
    bl_label = "Hoogtedata"

    def draw(self, context):
        layout = self.layout
        layout.menu('VIEW3D_MT_menu_hoogtedata_AHN', icon="VIEW_PERSPECTIVE")
        layout.menu('VIEW3D_MT_menu_hoogtedata_BAG3D', icon="HOME")
        layout.separator()
        layout.menu('VIEW3D_MT_menu_hoogtedata_Misc', icon="TOOL_SETTINGS")
        #layout.separator()
        #layout.menu('VIEW3D_MT_menu_hoogtedata_Experimental', icon="EXPERIMENTAL")

class VIEW3D_MT_menu_hoogtedata_AHN(Menu):
    bl_idname = "VIEW3D_MT_menu_hoogtedata_AHN"
    bl_label = "AHN (heightmap)"

    def draw(self, context):
        layout = self.layout
        layout.operator("gis.get_ahn", icon="VIEW_PERSPECTIVE")
        layout.separator()
        row = layout.row()
        row.label(text="AHN data via PDOK")
        row = layout.row()
        row.label(text="CC0, No Rights Reserved")

class VIEW3D_MT_menu_hoogtedata_BAG3D(Menu):
    bl_idname = "VIEW3D_MT_menu_hoogtedata_BAG3D"
    bl_label = "3D BAG (3d buildings)"

    def draw(self, context):
        layout = self.layout
        layout.operator("gis.get_bag", icon="HOME")
        layout.separator()
        row = layout.row()
        row.label(text="3D BAG by 3D geoinformation research group")
        row = layout.row()
        row.label(text="CC BY 4.0, Attribution")
        
class VIEW3D_MT_menu_hoogtedata_Misc(Menu):
    bl_idname = "VIEW3D_MT_menu_hoogtedata_Misc"
    bl_label = "Utils"

    def draw(self, context):
        layout = self.layout
        layout.operator("gis.create_sticky_material", icon="MATERIAL")
        layout.operator("gis.get_coordinate_info", icon="SPHERE")
        
class VIEW3D_MT_menu_hoogtedata_Experimental(Menu):
    bl_idname = "VIEW3D_MT_menu_hoogtedata_Experimental"
    bl_label = "Experimental"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text="Nothing yet :)")


# Register
classes = [
    VIEW3D_MT_menu_hoogtedata,
    VIEW3D_MT_menu_hoogtedata_AHN,
    VIEW3D_MT_menu_hoogtedata_BAG3D,
    VIEW3D_MT_menu_hoogtedata_Misc,
    VIEW3D_MT_menu_hoogtedata_Experimental,
    op_get_ahn.op_get_ahn,
    op_get_bag.op_get_bag,
    op_misc.op_create_sticky_material,
    op_misc.op_get_coordinate_info
    ]

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)
    # Adds submenu in View3D > Seams to Sewing Pattern
    bpy.types.VIEW3D_MT_menu_gis.append(menu_func)
    


def unregister():
    
    bpy.types.VIEW3D_MT_menu_gis.remove(menu_func)
    # Removes submenu
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)

if __name__ == "__main__":
    register()
