import bpy
from collections import defaultdict
from bpy.types import Operator
import bmesh
import mathutils
import math
from bpy.props import (
        StringProperty,
        BoolProperty,
        IntProperty,
        FloatProperty,
        FloatVectorProperty,
        EnumProperty,
        )

from . import utils

import urllib.request
import bmesh
import random

def get_pixel_at(x,y,w,h):
    x = max(0, min(x, w-1))
    y = max(0, min(y, h-1))
    return (( y * w + x ) * 4)

class op_get_ahn(Operator):
    bl_idname = "gis.get_ahn"
    bl_label = "Get AHN"
    bl_description = "Download AHN height data based on Basemap"
    bl_options = {'REGISTER', 'UNDO'}
    
    ahn_type: EnumProperty(
        name="AHN Type",
        description="",
        items=(
            ('DSM05', "DSM 0.5m (includes buildings)", "0.5m per pixel\nRaw height (includes buildings, vegetation, etc)"),
            ('DTM05', "DTM 0.5m (ground level)", "0.5m per pixel\nGround level (maaiveld)"),
            #('DSM5', "DSM 5m (includes buildings)", "5m per pixel\nRaw height (includes buildings, vegetation, etc)"),
            #('DTM5', "DTM 5m (ground level)", "5m per pixel\nGround level (maaiveld)"),
        ),
        default='DSM05',
    )
            
    nodata_cleanup_img: EnumProperty(
        name="Cleanup Img",
        description="The AHN contains holes.\nBefore the mesh is created, how do you want to clean it up?\n\nDTM contains more holes, as every building is cut out.\nDSM contains less holes.",
        items=(
            ('CRSFLT', "Crisscross and flatten (fast)", ""),
            
            ('JF_MIN', "Jumpflood Min (best for DSM, slow)", ""),
            ('JF_AVG', "Jumpflood Average (best for DTM, slow)", ""),
            ('CRSCRS', "Crisscross (fast)", ""),

            ('ZERO', "Reset to zero, (fast)", ""),
            ('NOTHING', "Do nothing, (fastest)", "")
        ),
        default='CRSFLT',
    )
    
    resolution_scale: FloatProperty(
        name="Resolution scale",
        description="100% = full resolution\n50% = half resolution\n\nRule of thumb:\n\nA resolution of 1000x1000 is a good starting point\nUse whole divisions (1/2, 1/3, 1/10) for best results\nYou can always start small and increase later\n",
        default=100,
        subtype="PERCENTAGE",
        min=0,
        max=100
    )
    
    cleanup_iterations: IntProperty(
        name="Cleanup Iterations",
        description="",
        default=10,
        min=0,
        max=100
    )


    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=350)

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        if(utils.is_scene_georef()):
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="Get Dutch height data to", icon='INFO')
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="match your existing basemap")
            layout.separator()
            layout.row()
            layout.row()
            
            row = layout.row()             
            scene_scale = utils.get_scene_scale_m()
            scene_scale_str = '%.1f'%scene_scale[0] + ', ' + '%.1f'%scene_scale[1]
            row.label(text="Scene size: " + scene_scale_str + 'm')
            
            row = layout.row()
            row.label(text="Download:", icon='URL')
            row = layout.row()
            row.prop(self, "ahn_type")
            row = layout.row()
            row.prop(self, "resolution_scale")
            
            
            ahn_resolution = 0.5
            if self.ahn_type == 'DTM5' or self.ahn_type == 'DSM5':
                ahn_resolution = 5
            
            row = layout.row()             
            scene_res = utils.get_scene_resolution_px(self.resolution_scale, ahn_resolution)
            scene_res_str = str(scene_res[0]) + ', ' + str(scene_res[1])
            row.label(text="Download resolution: " + scene_res_str + 'px')
            layout.row()
            

            row = layout.row()
            row.prop(self, "nodata_cleanup_img")
        else:
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="Scene has no basemap!", icon='ERROR')
            row = layout.row()
            row.scale_y = 0.6
            row.label(text="use (GIS > Web geodata > Basemap)")

    def execute(self, context):
        if not utils.is_scene_georef():
            self.report({'ERROR'}, "Can't get height data without a basemap present")
            return {'CANCELLED'}
        else:
            
            print("\n\n########## BLENDER HOOGTEDATA ADDON ##########\n\n")
            print("Starting to fetch AHN3 data")
            
            wm = bpy.context.window_manager
            wm.progress_begin(0, 100)
            

            utils.ensure_basemap_scale()
            basemap = utils.get_basemap_obj()

            img = basemap.data
            img.pack()
            

            scale = utils.get_scene_scale_m()

            dLon = scale[0] * 0.5
            dLat = scale[1] * 0.5

            basemap.location.z = 0;
            shiftLon = basemap.location.x
            shiftLat = basemap.location.y

            lon = bpy.context.scene["longitude"]
            lat = bpy.context.scene["latitude"]

            rd = utils.Rijksdriehoek()
            rd.from_wgs(lat,lon)

            lon = rd.rd_x + shiftLon
            lat = rd.rd_y + shiftLat
            
            print("Mercator origin: " + str(lon) + ", " + str(lat))

            bbox = str(lon-dLon)+","+str(lat-dLat)+","+str(lon+dLon)+","+str(lat+dLat)
            
            print("Bounding box: " + str(bbox))

            ahn_resolution = 0.5            
            if self.ahn_type == 'DTM5' or self.ahn_type == 'DSM5':
                ahn_resolution = 5

            scene_res = utils.get_scene_resolution_px(self.resolution_scale, ahn_resolution)
            
            print("Scene resolution in pixels: " + str(scene_res))

            #create a coverage key like ahn3_5m_dtm
            
            ahnkey = ''
            if self.ahn_type == 'DTM5' or self.ahn_type == 'DTM05':
                ahnkey += 'dtm'
            else:
                ahnkey += 'dsm'

            if self.ahn_type == 'DTM5' or self.ahn_type == 'DSM5':
                ahnkey += '_5m'
            else:
                ahnkey += '_05m'

            wm.progress_update(1)
            
            print("Creating a grid with the same resolution as the to-be-fetched heightmap")

            bpy.ops.mesh.primitive_grid_add(x_subdivisions=(scene_res[0]-1), y_subdivisions=(scene_res[1]-1), size=2.0, calc_uvs=True, enter_editmode=False, align='WORLD', location=(0.0, 0.0, 0.0), rotation=(0.0, 0.0, 0.0), scale=(0.0, 0.0, 0.0))
            grid = (bpy.context.selected_objects[-1])
            grid.scale = (dLon, dLat, 1)
            grid.location = basemap.location
            bpy.ops.object.transform_apply(location=False, rotation=False, scale=True, properties=False)
            bpy.context.active_object.name = ahnkey
            
            wm.progress_update(2)
            
            print("Requesting basemap from geodata.nationaalgeoregister.nl")
            
            url_params = [
                'https://service.pdok.nl/rws/ahn/wcs/v1_0/',
                '?request=GetCoverage&service=wcs',
                '&version=1.0.0',
                '&coverage={ahnkey}'
                '&crs=EPSG:7415'
                '&response_crs=EPSG:4326'
                '&bbox={bbox}',
                '&width={width}',
                '&height={height}',
                '&FORMAT=image/tiff',
                '&Interpolation=NEAREST'
            ]

            url = ''.join(url_params)
            url = url.replace("{bbox}", bbox)
            url = url.replace("{width}", str(scene_res[0]))
            url = url.replace("{height}", str(scene_res[1]))
            url = url.replace("{ahnkey}", ahnkey)

            print(url)                        

            #url = urllib.parse.quote(url, safe='/:?&')
                    
            try:
                req = urllib.request.Request(url, None, utils.get_request_header())
                handle = urllib.request.urlopen(req, timeout=10)
                data = handle.read()
                handle.close()
            
            except Exception as e:
                self.report({'ERROR'}, "Something went wrong fetching the heightmap...\n" + str(e) )
                return {'CANCELLED'}
            
            print("Heightmap downloaded.")
            
            wm.progress_update(3)
            
            file_path = utils.get_tmp_path() + ahnkey + '-' + bbox +'.tiff'
            
            print("Writing file to: " + file_path)

            tifffile = open(file_path,'wb') 
            tifffile.write(data)
            tifffile.close()
            
            print("Loading saved TIFF into Blender")
            print("Note: 'TIFFReadDirectory: Warning, Unknown field with tag' warnings are harmless")

            img = bpy.data.images.load(file_path)
            img.name =  ahnkey + '-' + bbox
            img.colorspace_settings.name = 'Non-Color'
            img.use_half_precision = False
            
            w = img.size[0]
            h = img.size[1]
                        
            local_pixels = list(img.pixels[:])
            
            bad_indexes = list()
            
            print("Identifying bad (nodata) pixels")
            
            for x in range(img.size[0]):
                for y in range(img.size[1]):
                    if(local_pixels[get_pixel_at(x,y,w,h)] > 500):
                        bad_indexes.append((int(x),int(y)))
                        local_pixels[get_pixel_at(x,y,w,h)+3] = 0
                        
            
            print("Attempting to clean up image using " + str(self.nodata_cleanup_img))
                        
                        
            ## JUMPFLOOD FILL METHOD
            
            if(self.nodata_cleanup_img == 'JF_AVG' or self.nodata_cleanup_img == 'JF_MIN'):            
                swap = False
                
                # JUMPER
                for itt in range(6,0,-1):
                    jf_distance = itt
                    for it in range(jf_distance):
                        
                        
                        
                        progress = 10*itt+it
                        wm.progress_update(progress)
                        
                        print("still cleaning up: " + str(progress))

                        swap = not swap
                        pxid = 1 if swap else 2
                        pxid2 = 2 if swap else 1
                        
                        p = int(2**((jf_distance-1)-it))
                        if(p < 1):
                            continue
                        
                        for bi in bad_indexes:    
                            lx = bi[0]
                            ly = bi[1]          
                            
                            vals = list()
                    
                            
                            #q = local_pixels[get_pixel_at(lx,ly,w,h)+pxid]
                            a = local_pixels[get_pixel_at(lx+p  ,ly  ,w,h)+pxid]
                            b = local_pixels[get_pixel_at(lx-p  ,ly  ,w,h)+pxid]
                            c = local_pixels[get_pixel_at(lx ,ly+p   ,w,h)+pxid]
                            d = local_pixels[get_pixel_at(lx ,ly-p   ,w,h)+pxid]
                            A = local_pixels[get_pixel_at(lx+p  ,ly+p  ,w,h)+pxid]
                            B = local_pixels[get_pixel_at(lx-p  ,ly+p  ,w,h)+pxid]
                            C = local_pixels[get_pixel_at(lx-p ,ly+p   ,w,h)+pxid]
                            D = local_pixels[get_pixel_at(lx-p ,ly-p   ,w,h)+pxid]
                            
                            z = min(a,b,c,d,A,B,C,D)
                            
                            if self.nodata_cleanup_img == 'JF_MIN':
                                local_pixels[get_pixel_at(lx,ly,w,h)+pxid2] = z
                            
                            else:
                                va = 1/(a - z + 1) if a < 500 else 0
                                vb = 1/(b - z + 1) if b < 500 else 0
                                vc = 1/(c - z + 1) if c < 500 else 0
                                vd = 1/(d - z + 1) if d < 500 else 0
                                vA = 1/(A - z + 1) if A < 500 else 0
                                vB = 1/(B - z + 1) if B < 500 else 0
                                vC = 1/(C - z + 1) if C < 500 else 0
                                vD = 1/(D - z + 1) if D < 500 else 0
                                
                                tot = a*va + b*vb + c*vc + d*vd + A*vA + B*vB + C*vC + D*vD
                                div = va+vb+vc+vd+vA+vB+vC+vD
                                
                                if(div > 0):                              
                                    local_pixels[get_pixel_at(lx,ly,w,h)+pxid2] = tot / div
                        
                            
                    swap = not swap
                    pxid = 1 if swap else 2
                    pxid2 = 2 if swap else 1
                    for bi in bad_indexes:
                        lx = bi[0]
                        ly = bi[1]
                        p = 1
                        a = local_pixels[get_pixel_at(lx+p  ,ly  ,w,h)+pxid]
                        b = local_pixels[get_pixel_at(lx-p  ,ly  ,w,h)+pxid]
                        c = local_pixels[get_pixel_at(lx ,ly+p   ,w,h)+pxid]
                        d = local_pixels[get_pixel_at(lx ,ly-p   ,w,h)+pxid]
                        z = (a+b+c+d)/4
                        local_pixels[get_pixel_at(lx,ly,w,h)+pxid2] = z           
            
        
                for bi in bad_indexes:
                    lx = bi[0]
                    ly = bi[1]
                    z = local_pixels[get_pixel_at(lx,ly,w,h)+pxid2]
                    local_pixels[get_pixel_at(lx,ly,w,h)] = z
                    local_pixels[get_pixel_at(lx,ly,w,h)+1] = z
                    local_pixels[get_pixel_at(lx,ly,w,h)+2] = z
            
            if(self.nodata_cleanup_img == 'ZERO'):
                for bi in bad_indexes:    
                    lx = bi[0]
                    ly = bi[1]                  
                    local_pixels[get_pixel_at(lx,ly,w,h)] = 0
                    local_pixels[get_pixel_at(lx,ly,w,h)+1] = 0
                    local_pixels[get_pixel_at(lx,ly,w,h)+2] = 0
               
            ''' UNUSED DILATE
            if(self.nodata_cleanup_img == 'DILATE'):
                swap = False
                for x in range(50):
                    wm.progress_update(50-x)
                    swap = not swap
                    pxid = 1 if swap else 2
                    pxid2 = 2 if swap else 1
                    r = random.randint(-1,1)
                    for bi in bad_indexes:    
                        lx = bi[0]
                        ly = bi[1]   
                        a = local_pixels[get_pixel_at(lx+1  ,ly-r  ,w,h)+pxid]
                        b = local_pixels[get_pixel_at(lx-1  ,ly+r  ,w,h)+pxid]
                        c = local_pixels[get_pixel_at(lx+r ,ly+1   ,w,h)+pxid]
                        d = local_pixels[get_pixel_at(lx-r ,ly-1   ,w,h)+pxid]
                        local_pixels[get_pixel_at(lx  ,ly  ,w,h)+pxid2] = min(a,b,c,d)
                for bi in bad_indexes:
                    lx = bi[0]
                    ly = bi[1]
                    z = local_pixels[get_pixel_at(lx,ly,w,h)+pxid2]
                    local_pixels[get_pixel_at(lx,ly,w,h)] = z
                    local_pixels[get_pixel_at(lx,ly,w,h)+1] = z
                    local_pixels[get_pixel_at(lx,ly,w,h)+2] = z
            
            '''
            
            if self.nodata_cleanup_img == 'CRSCRS' or self.nodata_cleanup_img == 'CRSFLT': #crisscross
    
                it = 0
                for bi in bad_indexes:
                    if it%2000 == 0:
                        wm.progress_update(int(it/len(bad_indexes)*100))
                    if it%10000 == 0:
                        print("still cleaning up: " + str(int(it/len(bad_indexes)*100)))
                    it+=1
                    
                    lx = bi[0]
                    ly = bi[1]
                    if local_pixels[get_pixel_at(lx  ,ly  ,w,h)+1] > 500:
                        val = 0
                        rxp = 0;
                        rxn = 0;
                        
                        for xp in range(scene_res[0]):
                            xpval = local_pixels[get_pixel_at(lx+xp  ,ly  ,w,h)+1]
                            rxp = int(xp)
                            if xpval < 500:
                                val = xpval
                                break
                        for xn in range(scene_res[0]):
                            xnval = local_pixels[get_pixel_at(lx-xn  ,ly  ,w,h)+1]
                            rxn = int(xn-1)
                            if xnval < 500:
                                val = min(val,xnval)
                                break
                        
                        for xx in range(-rxn, rxp):
                            local_pixels[get_pixel_at(lx+xx  ,ly  ,w,h)+1] = val
                    
                    if local_pixels[get_pixel_at(lx  ,ly  ,w,h)+2] > 500:
                        val = 0
                        ryp = 0;
                        ryn = 0;
                        
                        for yp in range(scene_res[1]):
                            ypval = local_pixels[get_pixel_at(lx  ,ly+yp  ,w,h)+2]
                            ryp = int(yp)
                            if ypval < 500:
                                val = ypval
                                break
                        for yn in range(scene_res[1]):
                            ynval = local_pixels[get_pixel_at(lx  ,ly-yn  ,w,h)+2]
                            ryn = int(yn-1)
                            if ynval < 500:
                                val = min(val,ynval)
                                break
                        
                        for yy in range(-ryn, ryp):
                            local_pixels[get_pixel_at(lx  ,ly+yy  ,w,h)+2] = val
                            
                for bi in bad_indexes:
                    lx = bi[0]
                    ly = bi[1]
                    v = min(local_pixels[get_pixel_at(lx,ly,w,h)+1], local_pixels[get_pixel_at(lx,ly,w,h)+2])
                    local_pixels[get_pixel_at(lx,ly,w,h)] = v
            
                        
            img.pixels = local_pixels[:]
            img.pack()

            

            wm.progress_update(4)

            obj = bpy.context.active_object

            #create a new modifier
            
            print("Displacing grid using modifier")

            displacer = obj.modifiers.new('AHN_Displace', type='DISPLACE')
            demTex = bpy.data.textures.new('demText', type = 'IMAGE')
            demTex.image = img
            demTex.use_interpolation = False
            demTex.extension = 'EXTEND'
            demTex.use_clamp = False
            displacer.texture = demTex
            displacer.texture_coords = 'UV'
            displacer.uv_layer = 'UVMap'
            displacer.mid_level = 0
            
            wm.progress_update(5)

            #apply the modifier

            bpy.ops.object.modifier_apply(modifier='AHN_Displace')
            
            wm.progress_update(6)

            print("Fixing all remaining bad (nodata) verticies")

            context = bpy.context

            bpy.ops.object.mode_set(mode = 'OBJECT')
            obj = bpy.context.active_object
            bpy.ops.object.mode_set(mode = 'EDIT') 
            ob = context.object
            me = ob.data
            bm = bmesh.from_edit_mesh(me)
            bm.select_mode = {'VERT'}
            bpy.ops.mesh.select_all(action='DESELECT')
            for i, v in enumerate(bm.verts):
                if v.co.z > 500:
                    v.co.z = 0
            bm.select_flush_mode()
            
            wm.progress_update(7)
            
            bpy.ops.mesh.select_mode(type='VERT')
            
            print("Selecting all verts which used to be nodata")
                
            for i, v in enumerate(bm.verts):
                vx = v.co.x
                vy = v.co.y
                
                
                vx /= dLon*2
                vy /= dLat*2
                vx += 0.5
                vy += 0.5
                vx *= scene_res[0]
                vy *= scene_res[1]
                
                alpha = local_pixels[get_pixel_at(int(vx), int(vy), w, h)+3]
                
                if alpha  < 0.5:
                    v.select_set(True)
                    
            bmesh.update_edit_mesh(me)
            
            wm.progress_update(8)
            
            if(self.nodata_cleanup_img == 'CRSFLT'):
                print("Flattening crisscross")
                tmp_transform_pivot = bpy.context.scene.tool_settings.transform_pivot_point
                bpy.context.scene.tool_settings.transform_pivot_point = 'INDIVIDUAL_ORIGINS'
                bpy.ops.object.mode_set(mode = 'OBJECT')
                bpy.ops.object.mode_set(mode = 'EDIT')        
                bpy.ops.transform.resize(value=(1.0, 1.0, 0.0), orient_type='GLOBAL', use_proportional_edit=False)
                bpy.context.scene.tool_settings.transform_pivot_point = tmp_transform_pivot
                
            wm.progress_update(9)
            
            bmesh.update_edit_mesh(me)
            
            bpy.ops.object.mode_set(mode = 'OBJECT')
            
            print("All done!")
            print("\n\n########## # ##########\n\n")
            
            return {'FINISHED'}
        


            
